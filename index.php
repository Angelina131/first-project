<?php //require_once $_SERVER["DOCUMENT_ROOT"]."/parsing.php";
session_start();

        // Код роутера
        class uSitemap {
        public $title = '';
        public $params = null;
        public $classname = '';
        public $data = null;

        public $request_uri = '';
        public $url_info = array();

        public $found = false;

        function __construct() {
        $this->mapClassName();
        }

        function mapClassName() {

        $this->classname = '';
        $this->title = '';
        $this->params = null;

        $map = array (
            '_404' => '404.php',   // Страница 404
            '/' => 'main.php',   // Главная страница
            '/basket' => 'basket.php',   // Корзина
            '/korm-dlya-koshek(/[0-9]+)' => 'product.php',
            '/korm-dlya-sobak(/[0-9]+)' => 'product.php',
            '/korm-dlya-gryzunov(/[0-9]+)' => 'product.php',
            '/korm-dlya-ptits(/[0-9]+)' => 'product.php',// С id-параметром товара
            '/korm-dlya-sobak' => 'catalog.php',
            '/korm-dlya-koshek' => 'catalog.php',
            '/korm-dlya-gryzunov' => 'catalog.php',
            '/korm-dlya-ptits' => 'catalog.php',
        );
        $this->request_uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $this->url_info = parse_url($this->request_uri);
        $uri = urldecode($this->url_info['path']);
        $data = false;
        foreach ($map as $term => $dd) {
        $match = array();
        $i = preg_match('@^'.$term.'$@Uu', $uri, $match);
        if ($i > 0) {
        // Get class name and main title part
        $m = explode(',', $dd);
        $data = array(
        'classname' => isset($m[0])?strtolower(trim($m[0])):'',
        'title' => isset($m[1])?trim($m[1]):'',
        'params' => $match,
        );
        break;
        }
        }
        if ($data === false) {
        // 404
        if (isset($map['_404'])) {
        // Default 404 page
        $dd = $map['_404'];
        $m = explode(',', $dd);
        $this->classname = strtolower(trim($m[0]));
        $this->title = trim($m[1]);
        $this->params = array();
        }
        $this->found = false;
        } else {
        // Found!
        $this->classname = $data['classname'];
        $this->title = $data['title'];
        $this->params = $data['params'];
        $this->found = true;
        }
        return $this->classname;
        }
        }
        $sm = new uSitemap();
        $routed_file = $sm->classname; // Получаем имя файла для подключения через require()
        $GLOBALS['page-params'] = $sm->params;
        include('./'.$routed_file); // Подключаем файл






