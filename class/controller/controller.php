<?php


class MainController
{

    public static function GetUrlPage(){
        $page=$_SERVER['REQUEST_URI'];
        $string_url_1=explode("/?page=", $page);
        return $string_url_1[0];
    }

    public static function GetCurPage(){
        $page=$_SERVER['REQUEST_URI'];
        $string_url_1=explode("/?page=", $page);
        return $string_url_1[1];
    }

    public static function GetCategory(){
        $temp=self::GetUrlPage();
        $string_url_2=explode("/",$temp);
        return $string_url_2[1];
    }

    public static function UrlSection(){
        switch ($GLOBALS['page-params'][0]){
            case "/korm-dlya-sobak":
                {
                    $id='99';
                    break;
                }
            case"/korm-dlya-koshek":
                {
                    $id='100';
                    break;
                }
            case"/korm-dlya-gryzunov":
                {
                    $id='101';
                    break;
                }
            case"/korm-dlya-ptits":
                {
                    $id='102';
                    break;
                }
        }
        return $id;
    }

    public static function NameSection(){
        switch ($GLOBALS['page-params'][0]){
            case "/korm-dlya-sobak":
                {
                    $name='собакам';
                    break;
                }
            case"/korm-dlya-koshek":
                {
                    $name='кошкам';
                    break;
                }
            case"/korm-dlya-gryzunov":
                {
                    $name='грызунам';
                    break;
                }
            case"/korm-dlya-ptits":
                {
                    $name='птицам';
                    break;
                }
        }
        return $name;
    }

    public static function IdProduct(){
        $id_product=ltrim($GLOBALS['page-params'][1], "/");
        return $id_product;
    }

    public static function NoIdUrl($url)
    {
    $string_arr=explode("/",$url);
    return $string_arr[0] . '/' . $string_arr[1];
    }


    public static function Breadcrumbs(){
            $cur_url=$_SERVER['REQUEST_URI'];
            $urls=explode('/',$cur_url);
            $crumbs=array();
            if(!empty($urls)&& $cur_url!='/'){
                foreach($urls as $key=>$value){

                    $prev_urls=array();

                    for($i=0; $i <= $key; $i++){

                        $prev_urls[]=$urls[$i];
                    }

                    if($key==count($urls)-1){
                        $crumbs[$key]['url']='';
                    }

                    elseif (!empty($prev_urls)){
                        $crumbs[$key]['url']=count($prev_urls) > 1 ? implode('/',$prev_urls):'/';
                    }

                    switch ($value) {
                        case 'korm-dlya-koshek' : $crumbs[$key]['text'] = 'Корм для кошек';
                            break;

                        case 'korm-dlya-sobak' : $crumbs[$key]['text'] = 'Корм для собак';
                            break;

                        case 'korm-dlya-gryzunov' : $crumbs[$key]['text'] = 'Корм для грызунов';
                            break;

                        case 'korm-dlya-ptits' : $crumbs[$key]['text'] = 'Корм для птиц';
                            break;

                        case 'basket' : $crumbs[$key]['text'] = 'Корзина';
                            break;

                        case '' : $crumbs[$key]['text'] = 'Главная';
                            break;
                    }
                }
            }
            return $crumbs;
    }


}

