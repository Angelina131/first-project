<?php

class SingleDB
{

    private static $link;

    private static $host = 'localhost';
    private static $database = 'zoomarket';
    private static $user = 'root';
    private static $password = '';

    private function __construct()
    {
    }

    public static function get_link()
    {
        if(empty(self::$link)){
            self::$link = mysqli_connect (self::$host, self::$user, self::$password, self::$database);
        }
        return self::$link;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

}

class queryDB extends SingleDB
{
    public static function get($sql){
        $resQuery=" ";
        $result = mysqli_query(parent::get_link() , $sql);
        if(!is_bool($result))$resQuery = mysqli_fetch_all($result, MYSQLI_ASSOC);
        if(empty($result)) $resQuery=false;
        return $resQuery;

    }
}
