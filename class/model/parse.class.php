<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/class/model/singleton.php";

class ParseClass
{
    public static function load($file_name)
    {
        if (file_exists($file_name)) {
            return $xml = simplexml_load_file($file_name);
        } else {
            exit('Не удалось открыть файл ' . $file_name);
        }
    }


    public static function add_section($value)
    {
        $section = array(
            'id' => $value->id->__toString(),
            'name' => $value->name->__toString(),
        );
        return queryDB::get("INSERT INTO `section` VALUES ('$section[id]', '$section[name]')");
    }


    public static function add_product($about_product)
    {
        $product = array(
            'id' => $about_product->id->__toString(),
            'name' => $about_product->name->__toString(),
            'description' => ltrim(rtrim($about_product->description->asXML(),"<description>"),"</description>"),
            'id_section' => $about_product->id_section->__toString(),
            'image'=> $about_product->image->__toString(),
            'price'=>rand(100,1000),
        );

        if (!$product['id']){
            $last_id = queryDB::get(("SELECT MAX(`id`) FROM `product`"));
            var_dump($last_id[0][0]);
            $product['id'] = $last_id[0][0] + 1;
        }

        foreach ($about_product->characteristics->characteristic as $item) {
            self::add_char_value($product['id'],$item);
        }

        return queryDB::get("INSERT INTO `product` VALUES ('$product[id]', '$product[name]', '$product[description]', '$product[id_section]', '$product[image]', '$product[price]')");
    }



    public static function add_char_value($product_id,$item)
    {
        $character = array(
            'id' => $item->id->__toString(),
            'value' => $item->value->__toString(),
        );
        return queryDB::get("INSERT INTO `char_value` (`id`, `id_char`, `id_product`, `value_char`)  VALUES ('', '$character[id]', '$product_id' ,'$character[value]');");
    }



    public static function add_characteristic($char)
    {
        $characteristic = array(
            'id' => $char->id->__toString(),
            'name' => $char->name->__toString(),
        );
        return queryDB::get("INSERT INTO `characteristic` VALUES ('$characteristic[id]', '$characteristic[name]')");
    }


}