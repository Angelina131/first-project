<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/class/model/singleton.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/class/controller/controller.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/class/controller/controller.basket.php";

class GetInfo
{

    public static function GetSection()
    {
        $section_list = "SELECT * FROM `section`";
        $section = queryDB::get($section_list);
        return $section;
    }

    public static function GetNameSection($id_section)
    {
        $section_list = "SELECT * FROM `section` WHERE `id_section`=$id_section";
        $section = queryDB::get($section_list);
        return $section;
    }

    public static function GetProductId($id, $start, $perpage)
    {
        $product_list = "SELECT * FROM `product` WHERE `id_section`='$id'  LIMIT $start, $perpage";
        $product = queryDB::get($product_list);
        return $product;
    }

    public static function GetProductAll($id_product)
    {
        $products = "SELECT * FROM `product`  WHERE `product`.`id_product`=$id_product ";
        $product = queryDB::get($products);
        return $product;
    }

    public static function GetCountProduct($id)
    {
        $select = "SELECT count(*) FROM `product` WHERE `id_section`='$id'";
        $total = queryDB::get($select);
        return $total[0];
    }

    public static function GetChar($id_product)
    {
        $characteristics = "SELECT * FROM `characteristic` INNER JOIN `char_value` ON (characteristic.id_char=char_value.id_char) WHERE `char_value`.`id_product`=$id_product";
        $characteristic = queryDB::get($characteristics);
        return $characteristic;
    }

    public static function GetProductNoId($id_section)
    {
        $all_product = "SELECT * FROM `product` WHERE `id_section`=$id_section ORDER BY RAND() LIMIT 5";
        $sql_product = queryDB::get($all_product);
        return $sql_product;
    }

    public static function AddProductUser($cookie, $id, $qty)
    {
        $goods = "INSERT INTO `basket` VALUE ('','{$cookie}' , {$id}, {$qty})";
        if (queryDB::get($goods))
            return 'ok';
        else return 'error!';
    }

    public static function GetBasket($cookie)
    {
        $all_info = "SELECT * FROM `basket` WHERE `id_users`='{$cookie}'";
        $product_basket = queryDB::get($all_info);
        return $product_basket;
    }

    public static function InsertBasket($id, $user_id, $qty)
    {
        $insert_product = "UPDATE `basket`  SET `quantity` = {$qty} WHERE `basket`.`id_users` = '{$user_id}' AND `basket`.`id_product`= {$id}";
        if (queryDB::get($insert_product))
            return 'ok';
        else return 'error!';
    }

    public static function DeleteBasket($user_id)
    {
        $delete="DELETE FROM `basket` WHERE `basket`.`id_users` = '{$user_id}'";
        if (queryDB::get($delete))
            return queryDB::get($delete);
        else return 'error!';
    }
    public static function DeleteProduct($user_id,$id_row)
    {
        $del_product="DELETE FROM `basket` WHERE `basket`.`id_users` = '{$user_id}' and `basket`.`id`='{$id_row}'";
        if (queryDB::get($del_product))
            return queryDB::get($del_product);
        else return 'error!';
    }

    public static function CountGoodBasket($user_id)
    {
        $count= "SELECT SUM(quantity) FROM `basket` WHERE `id_users`='{$user_id}'";
        $quantity=queryDB::get($count);
        return implode($quantity[0]);
    }

    public static function GetPrice($id)
    {
        $sql_price="SELECT `price` FROM `product` WHERE `id_product`=$id";
        $price=queryDB::get($sql_price);
        return $price[0]['price'];

    }


   public static function TotalSum($user_id)
    {
        $sum=0;
        $sql="SELECT * FROM `basket` WHERE `id_users`='{$user_id}'";
        $items=queryDB::get($sql);
        foreach($items as $item){
            $price=(int)self::GetPrice($item['id_product']);
            $sum=$sum + ($price*$item['quantity']);

        }
        return( $sum);
    }

}
