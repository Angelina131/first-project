<?php require_once $_SERVER["DOCUMENT_ROOT"]."/class/model/get.info.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/class/model/singleton.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/class/controller/controller.php";?>



<div class="container">
    <?php
    $crumbs=MainController::Breadcrumbs();
    $url=MainController::GetUrlPage();
    $id_product=MainController::IdProduct(); // достаем id из url
    $product=GetInfo::GetProductAll($id_product);
    $character=GetInfo::GetChar($id_product);
    if (!empty($crumbs)) { ?>
        <section id="inner-headline">
            <div class="row mx-0">
                <div class="col-lg-12 px-0">
                    <ul class="breadcrumb pl-0">
                        <?php foreach ($crumbs as $item) { ?>
                            <?php if (isset($item)) { ?>
                                <li>
                                    <?php if (!empty($item['url'])) { ?>
                                        <a href="<?php echo $item['url'] ?>"><?php echo $item['text'] ?></a>
                                        <span><i class="fa fa-chevron-right mx-2" aria-hidden="true"></i></span>
                                    <?php } else { ?>
                                        <?php echo $item['text'] ?>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        <?php } ?>
                        <? foreach ($product as $good){
                            echo"<span> $good[name] </span>";
                        }?>

                    </ul>
                </div>
            </div>
        </section>
    <?php } ?>
    <?php
     foreach ($product as $item){
         echo" <h2 class='mb-5'>$item[name]</h2>
               <div class='container px-0'>
                     <div class=\"photo_product\">
                       <div class='image'><img src=\"$item[image]\" alt=\"\" class=\"\"></div>
                 </div>
                 <div class=\"product_info\">
                      <span class=\"price_product\"> Цена: <span class='price_goods'>$item[price]  ₽</span> </span>
                      <div class=\"description\">$item[description]</div>
                 <div class=\"quantity\">
                     <div class=\"minus\"><i class=\"fa fa-minus\"  data-id=\"$item[id_product]\" aria-hidden=\"true\"></i></div>
                     <input type='text' class=\"quantity_sum\"  value=\"1\" data-id=\"$item[id_product]\" readonly>
                     <div class=\"plus\" data-id=\"$item[id_product]\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i></div>
                 </div>
                 <div class=\"buy_product add-to-cart\"  data-id=\"$item[id_product]\"><img src = \"/img/buy.png\" class=\"basket_buy\" alt = \"basket\"> В корзину</div >
       </div>
    </div>";}?>
</div>



<div class="container-fluid characteristic px-0">
    <p class="title">Характеристики</p>
        <?php  foreach($character as $char){
           echo"
              <div class=\"character mb-2\">
                 <div class=\"char pr-2\">$char[name]</div>
                 <div class=\"border_block\"></div>
                 <div class=\"char_value pl-2\">$char[value_char]</div>
              </div>";
         }?>
</div>

    <div class="container analogy_product px-0">
        <p class="title">Похожие товары</p>
        <div class="type d-flex justify-content-between flex-wrap">
            <?php
            $id_section= $product[0]['id_section'];
            $similar_product=GetInfo::GetProductNoId($id_section);
            $url_string=MainController::NoIdUrl($url);//получаем текущий раздел
            shuffle($similar_product);
            foreach ($similar_product as $similar){
            echo"<div class=\"product\" data-id=\"$similar[id_product]\">
                    <div class=\"product_name\">$similar[name]</div >
                    <div class=\"product_img\"><a href='$url_string/$similar[id_product]'><img src=\"$similar[image]\" alt=\"\"></a></div>
                    <div class=\"price\">$similar[price] ₽</div >
                    <div class=\"buy add-to-cart\" data-id=\"$similar[id_product]\"><img src = \"/img/buy.png\" class=\"basket_buy\" alt = \"basket\"> Купить</div>
                 </div >";
            }?>
        </div >
     </div>
    </div>

