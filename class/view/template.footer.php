
    <footer>
        <div class="container-fluid footer-background">
            <div class="container footer-info">
                <span class="footer-logo">
               <img src="/img/logo-foot.png" alt="logo" class="logo-footer">
                </span>
                <div class="nav-menu-footer">
                    <ul class="nav hd-nav-footer">
                        <li class="nav-item">
                            <a class="nav-link-title" href="/">Каталог:</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link-foot" href="/korm-dlya-sobak">Собакам</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link-foot" href="/korm-dlya-koshek">Кошкам</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link-foot" href="/korm-dlya-gryzunov">Грызунам</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link-foot" href="/korm-dlya-ptits">Птицам</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <script src="/js/bootstrap.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/js/script.js"></script>
    </body>
    </html>