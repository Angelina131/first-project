


<div class="container p-0">
    <h1 class="catalog-title">Каталог</h1>
    <div class="category d-flex justify-content-between flex-wrap">
        <div class="types ">
            <div class="top"></div>
            <span class="section">Корм собакам</span>
            <img src="/img/dog.png" alt="" class="section_img">
            <div class="details"><a href="/korm-dlya-sobak" class="">Подробнее</a></div>
        </div>
        <div class="types ">
            <div class="top"></div>
            <span class="section">Корм кошкам</span>
            <img src="/img/cat.png" alt="" class="section_img">
            <div class="details"><a href="/korm-dlya-koshek" class="">Подробнее</a></div>
        </div>
        <div class="types ">
            <div class="top"></div>
            <span class="section">Корм грызунам</span>
            <img src="/img/rub.png" alt="" class="section_img">
            <div class="details"><a href="/korm-dlya-gryzunov" class="">Подробнее</a></div>
        </div>
        <div class="types ">
            <div class="top"></div>
            <span class="section">Корм попугаям</span>
            <img src="/img/pop.png" alt="" class="section_img">
            <div class="details"><a href="/korm-dlya-ptits" class="">Подробнее</a></div>
        </div>


    </div>
</div>
