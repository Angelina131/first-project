<?php require_once $_SERVER["DOCUMENT_ROOT"]."/class/model/get.info.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/class/model/singleton.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/class/controller/controller.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/pagination.php";?>



    <?php
    $id=MainController::UrlSection(); //определяем id секции(раздела)
    $uu=GetInfo::GetCountProduct($id);
    $totals=GetInfo::GetCountProduct($id);
    $total=implode($totals);
    $perpage=20;
    $page=isset($_GET['page']) ? (int)$_GET['page'] : 1;
    $pagination = new Pagination($page, $perpage, $total);
    $start=$pagination->getStart();
    $products=GetInfo::GetProductId($id, $start, $perpage);
    $name_section=MainController::NameSection();
    $url=$GLOBALS['page-params'][0];
    $crumbs=MainController::Breadcrumbs();
    $page=MainController::GetCurPage(); //определяем номер страницы если это ?page=
    ?>
<div class="container container_all_product">
    <?php
    if (!empty($crumbs)) { ?>
        <section id="inner-headline">
            <div class="row mx-0">
                <div class="col-lg-12 px-0">
                    <ul class="breadcrumb pl-0">
                        <?php foreach ($crumbs as $item) { ?>
                            <?php if (isset($item)) { ?>
                                <li>
                                    <?php if (!empty($item['url'])) { ?>
                                        <a href="<?php echo $item['url'] ?>"><?php echo $item['text'] ?></a>
                                        <span><i class="fa fa-chevron-right mx-2" aria-hidden="true"></i></span>
                                    <?php } else { ?>
                                        <?php echo $item['text'] ?>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </section>
    <?php } ?>
    <h1>Корм <?=$name_section?></h1>
    <div class="type d-flex justify-content-between flex-wrap">
    <?php foreach ($products as $product) {
        $a=$url . '/' .$product['id_product'];
        echo " <div class=\"product\" data-id=\"$product[id_product]\">
                <div class=\"product_name\" >$product[name]</div >
                <div class=\"product_img\"><a href=\"$a\"><img src=\"$product[image]\" alt=\"\"></a></div>
                <div class=\"price\" >$product[price] ₽</div >
                <div class=\"buy add-to-cart\" data-id='$product[id_product]'><img src = \"/img/buy.png\" class=\"basket_buy\" alt = \"basket\" > Купить</div >
               </div >";
    }?>
    </div>
    <?php echo $pagination->getHtml();?>
</div>











