<?php
class ViewBasket
{
    public static function ShowCart($cookie){
        $all_info = GetInfo::GetBasket($cookie);?>
         <table class="table table-hover table_basket\">
            <thead>
            <tr class="head_table">
                <th scope="col" class=\"head_row\">ID</th>
                <th scope="col" class=\"head_row\">Фото</th>
                <th scope="col" class=\"head_row\">Наименование</th>
                <th scope="col" class=\"head_row\">Цена</th>
                <th scope="col" class=\"head_row\">Количество</th>
                <th scope="col" class=\"head_row\">Сумма</th>
                <th scope="col" class=\"head_row\"></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                if(!$all_info){
                    echo"<h3>Ваша корзина пуста. Перейти к <a href=\"/\">покупкам</a> </h3>";
                }
                foreach ($all_info as $items) {
                    $product_id=$items['id_product'];
                    $products=GetInfo::GetProductAll($product_id);;
                    $product=$products[0];
                    $total_price=$items['quantity']*$product['price'];
                    $itog_sum += $total_price;
                    //var_dump($itog_sum);
                    echo"<th scope=\"row\" class='id-row' data-id='$items[id]'>$items[id]</th>
                    <td><img class='basket-img' src='$product[image]' alt='$product[name]'></td>
                    <td class=\"name_product\">$product[name]</td>
                    <td class=\"price_p\" data-id=\"$items[id_product]\">$product[price] </td>
                    <td>
                        <div class=\"quantity\">
                            <div class=\"minusP\" data-id=\"$items[id_product]\"><i class=\"fa fa-minus\" aria-hidden=\"true\"></i></div>
                            <input type='text' class=\"quantity_sum_P\"  value=\"$items[quantity]\" data-id=\"$items[id_product]\" readonly>
                            <div class=\"plusP\"  data-id=\"$items[id_product]\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i></div>
                        </div>
                    </td>
                    <td><output  class=\"price_t\" data-id=\"$items[id_product]\">$total_price <span>р</span></output>  </td>
                    <td> <span class=\"del-product\" ><i class=\"fa fa-times\" aria-hidden=\"true\" data-id=\"$items[id]\"></i></span> </td>
                </tr>";
                }?>
            </tbody>
        </table>
        <div class="reset" > Очистить корзину </div >
        <div class="total-price">Итого: <span class="itog_price"><?php echo $itog_sum ?><span> рублей</span></span></div>

    <?php }}?>





