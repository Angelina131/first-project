<?php require_once $_SERVER["DOCUMENT_ROOT"]."/class/model/get.info.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/class/controller/controller.php";?>

<div class="container breadcrumb">
    <?php
    $product=GetInfo::GetProductAll($id_product);
    $crumbs=MainController::Breadcrumbs();
    if (!empty($crumbs)) { ?>
        <section id="inner-headline">
            <div class="row mx-0">
                <div class="col-lg-12 px-0">
                    <ul class="breadcrumb pl-0">
                        <?php foreach ($crumbs as $item) { ?>
                            <?php if (isset($item)) { ?>
                                <li>
                                    <?php if (!empty($item['url'])) { ?>
                                        <a href="<?php echo $item['url'] ?>"><?php echo $item['text'] ?></a>
                                        <span><i class="fa fa-chevron-right mx-2" aria-hidden="true"></i></span>
                                    <?php } else { ?>
                                        <?php echo $item['text'] ?>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </section>
    <?php } ?>
</div>
