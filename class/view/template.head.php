<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/adaptive.css">
    <title>Зоомаркет</title>
</head>
<body>



<header>
    <div class="container-fluid head-line">
    </div>
    <div class="container header-logo">
        <img src="/img/logo.png" alt="logo">
            <div class="basket-characteristic">
                <div class="basket-info">
                    <p class="title-basket mb-0"><a href="/basket">Корзина</a></p><br>
                    <span class="price-basket">Сумма <span class="total-price-head"></span></span>
                </div>
                <div class="basket-cart">
                    <img src="/img/basket.png" alt="basket" class="basket">
                    <output class="quantity_head"></output>
                </div>
            </div>
    </div>
<nav class="menu">
    <div class="container">
    <div class="nav-menu">
        <ul class="nav hd-nav">
            <li class="nav-item">
                <a class="nav-link hover-effect" href="/">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-link hover-effect" href="/korm-dlya-sobak">Корм собакам</a>
            </li>
            <li class="nav-item">
                <a class="nav-link hover-effect" href="/korm-dlya-koshek">Корм кошкам</a>
            </li>
            <li class="nav-item">
                <a class="nav-link hover-effect" href="/korm-dlya-gryzunov">Корм грызунам</a>
            </li>
            <li class="nav-item">
                <a class="nav-link hover-effect" href="/korm-dlya-ptits">Корм птицам</a>
            </li>
        </ul>
    </div>
    </div>
</nav>
</header>

