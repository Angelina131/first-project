<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/class/view/catalog.view.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/class/model/get.info.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/class/controller/controller.php";;

class Pagination
{

    public $currentPage;//текущая страница
    public $perpage;//сколько записей выводить на страницу
    public $total;//общее кол-во записей
    public $countPages;//общее кол-во страниц
    public $uri;//адрес

    public function __construct($page, $perpage,$total){
        $this->perpage=$perpage;
        $this->total=$total;
        $this->countPages=$this->GetCountPages();
        $this->currentPage=$this->getCurrentPage($page);
        $this->uri=$this->getParams();
}

    public function __toString()
    {
        return $this->getHtml();
    }

    public function getHtml(){
        $back=null;
        $forward=null;;
        $startpage=null;
        $showpage=null;
        $end=$this->countPages;
        $page=$_GET['page'];
        if(!isset($page))
            $page=1;

        if($this->currentPage == 1){
            $back= "<div class='inactive'><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i></div>";}
            else $back= "<div class='back'  data-id='".($this->currentPage - 1)."'><a href='{$this->uri}page=".($this->currentPage - 1)."' class='nav-links'><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i></a></div>";


       for($i=1; $i <= $end; $i++) {
           if ((in_array($page, range(3, $end - 2)) && in_array($i, range($page - 2, $page + 2))) ||
               (in_array($page, range(1, 2)) && in_array($i, range(1, 5)) ||
               (in_array($page, range($end - 1, $end)) && in_array($i, range($end - 4, $end))))) {
               $showpage .= "<div class='number_page";
               if ($i == $page) $showpage .= " active";
               $showpage .= "'> <a href='{$this->uri}page=" . $i . "' class='nav-links'>" . $i . "</a></div>";
           }
       }


        if($this->currentPage < $this->countPages){
            $forward="<div class='next' data-id='".($this->currentPage + 1)."'><a href='{$this->uri}page=".($this->currentPage + 1)."' class='nav-links'><i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></a></div>";
        }

        return '  <div class="container mt-5"> <input type="hidden" name="command"> <div class="pagins d-flex justify-content-center">' .$back .$showpage.$forward. '</div></div>';
    }


    public function getCountPages(){
        return ceil($this->total / $this->perpage) ?: 1; //Определяет сколько всего записей и сколько записей выводить на страницу.
    }

    public function getCurrentPage($page){ //проверка количества страниц с гет-параметрами
        if(!$page || $page < 1) $page=1;
        if($page > $this->countPages) $page = $this->countPages;
        return $page;
    }

    public function getStart(){ //определяет с какой записи начинать выборку
        return ($this->currentPage - 1) * $this->perpage;
    }

    public function getParams(){
        $url=$_SERVER['REQUEST_URI'];
        $url=explode('?',$url);
        $uri=$url[0] . '?';
        return $uri;
    }

}