$(document).ready(function() {

    //Работа с cookie
    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    if (!getCookie('user_id')){
        var comm = [];
        comm.push({name:"basket",value:"create-cookie"});
        $.post('/class/controller/controller.basket.php', comm)
            .done (function (result){
                if (result){
                    document.cookie = "user_id=" + result;
                }
                else{
                    alert ("Действие не выполнено.")
                }
            })

            .fail (function () {
                alert("Ошибка доступа к серверу");
            });
    }


    if (location.pathname == '/basket'){
        var comm = [];
        comm.push({name:"basket",value:"show-cart"});
        comm.push({name:"cookie-user-id",value:getCookie('user_id')});
        $.post('/class/controller/controller.basket.php', comm)

            .done (function (result) {
                if (result){
                    $(".hh > div").html(result);
                }
                else{
                    alert ("Действие не выполнено.")
                }
            })
            .fail (function () {
                alert("Ошибка доступа к серверу");
            });
    }

//Добавление товара в корзину в каталоге и карточке товара
    $('body').on("click", ".add-to-cart",function(){
        var data= [];
        var dataId=$(this).attr('data-id');
        var qty=$(".quantity input").val() ? $(".quantity input").val() : 1;
        data.push({name:'basket', value:'add-to-cart'}); //name,value зависят от данных, указанных в конструкции switch
        data.push({name:'id', value: dataId});
        data.push({name:'user_id', value: getCookie('user_id')});
        data.push({name:'qty', value: qty});
        console.log(data);
        $.ajax({
            type: 'POST',
            url: '/class/controller/controller.basket.php',
            data: data,
            success:function(result){
            }

        });
    });

//обновление количества в корзине при любом клике
    function cart_total_price_updating() {
        var comm = [];
        comm.push({name:'basket',value:'total-quantity'});
        comm.push({name:"user_id",value:getCookie('user_id')});
        $.post('/class/controller/controller.basket.php', comm)
            .done(function (result) {
                if (result) {
                    $(".quantity_head").html(result);
                } else {
                    alert("Действие не выполнено.")
                }
            })
            .fail(function () {
                alert("Ошибка доступа к серверу");
            });

    }

    cart_total_price_updating();
    $("body").on("click",cart_total_price_updating);

//обновление суммы товаров в корзине при любом клике
    function cart_amount_updating() {
        var comm = [];
        comm.push({name:'basket',value:'total-sum'});
        comm.push({name:"user_id",value:getCookie('user_id')});
        $.post("/class/controller/controller.basket.php", comm)
            .done(function (result) {
                if (result) {
                    $(".total-price-head").html(result);
                } else {
                    alert("Действие не выполнено.")
                }
            })
            .fail(function () {
                alert("Ошибка доступа к серверу");
            });
    }

    cart_amount_updating();
    $("body").on("click",cart_amount_updating);




    <!-- Добавление выбранного товара в корзине -->

    $('body').on("click",".plusP",function(){
        var data = [];
        var cookie=getCookie('user_id');
        var id=$(this).attr('data-id');
        var qty=$('.quantity_sum_P[data-id="'+ id +'"]').val();
        var price = $('.price_p[data-id="'+ id +'"]').html();
        var total_price = parseInt($('.price_t[data-id="'+ id +'"]').html());
        var totalHeadPrice=$('.total-price-head').html();
        var amount=$('.quantity_head').html();
        data.push({name:'basket', value:'insert-basket'}); //name,value зависят от данных, указанных в конструкции switch
        data.push({name:'id', value: id});
        data.push({name:'user_id', value: getCookie('user_id')});
        data.push({name:'qty', value: Number(qty) + 1});
        console.log(totalHeadPrice);
        $.ajax({
            type: 'POST',
            url: '/class/controller/controller.basket.php',
            data: data,
            success:function(result){
                var count= Number(qty) + 1;
                var sum= (Number(price) + Number(total_price));
                //console.log(sum);
                $('.price_t[data-id="' + id +'"]').text(Number(sum));
                $('.quantity_sum_P[data-id="'+ id +'"]').val(count);
                $('.itog_price').text(Number(totalHeadPrice) +Number(price) + ' рублей');
                $('.quantity_head').html(Number(amount) + 1);

            }

        });
    });

    <!--Удаление выбранного товара в корзине-->

    $('body').on("click",".minusP",function(){
        var data = [];
        var cookie=getCookie('user_id');
        var id=$(this).attr('data-id');
        var qty=$('.quantity_sum_P[data-id="'+ id +'"]').val();
        var price = $('.price_p[data-id="'+ id +'"]').html();
        var total_price = parseInt($('.price_t[data-id="'+ id +'"]').html());
        data.push({name:'basket', value:'insert-basket'}); //name,value зависят от данных, указанных в конструкции switch
        data.push({name:'id', value: id});
        data.push({name:'user_id', value: getCookie('user_id')});
        data.push({name:'qty', value: Number(qty) - 1});
        //console.log(total_price,price);
        $.ajax({
            type: 'POST',
            url: '/class/controller/controller.basket.php',
            data: data,
            success:function(result){
                var count= Number(qty) - 1;
                var sum= (Number(total_price) - Number(price));
                console.log(sum);
                $('.price_t[data-id="' + id +'"]').text(Number(sum));
                $('.quantity_sum_P[data-id="'+ id +'"]').val(count);
                $('.itog_price').text(Number(total_price)- Number(price)+ ' рублей');
                $('.quantity_head').text(Number(qty));

            }

        });
    });

    <!--Очистка корзины-->

    $('body').on("click",".reset",function(){
        var data = [];
        var cookie=getCookie('user_id');
        data.push({name:'basket', value:'reset-basket'});
        data.push({name:'user_id', value: getCookie('user_id')});
        //console.log(total_price,price);
        $.ajax({
            type: 'POST',
            url: '/class/controller/controller.basket.php',
            data: data,
            success:function(result){
                if (result){
                    $(".hh > div").html(result);
                    $(".reset").remove();
                    $(".head_table").remove();
                    $(".total-price").remove();
                    $(".quantity_head").text(0);
                }
                //console.log(".total-price");

            }

        });
    });


    <!--удаление  товара в корзине-->

    $('body').on("click",".fa-times",function(){
        var data = [];
        var id=$(this).attr('data-id');
        var cookie=getCookie('user_id');
        data.push({name:'basket', value:'delete-product'}); //name,value зависят от данных, указанных в конструкции switch
        data.push({name:'user_id', value: cookie});
        data.push({name:'id_row', value: id});
        console.log(data, id);
        $.ajax({
            type: 'POST',
            url: '/class/controller/controller.basket.php',
            data: data,
            success:function(result){
                if (result){
                    $(".hh > div").html(result);
                }
                //console.log(result);

            }

        });
    });


    <!--Добавление/удаление количества товара в карточке товара-->
    $(".plus").on("click",function(){
        var count = parseInt($('.quantity_sum').val());
        console.log(count);
        $('.quantity_sum').val(count + 1);
    });

    $(".minus").on("click", function (){
        var count = parseInt($('.quantity_sum').val());
        console.log(count);

        $('.quantity_sum').val(count - 1);

    });







});